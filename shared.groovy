env.TEST_3 = "XYZ"

GIT_COMMIT_VAR = GIT_COMMIT[0..6]

def setBuildDescription() {
    currentBuild.description = "Branch: ${GIT_BRANCH} (${GIT_COMMIT_VAR})"
}

def getTest3() {
    return "ABCDEF"
}

return this